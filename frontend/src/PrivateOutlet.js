/* eslint-disable react/prop-types */
/* eslint-disable no-undef */
import React from 'react'
import { Outlet, Navigate } from 'react-router-dom'
import { useSelector } from 'react-redux'

const PrivateOutlet = () => {
  const authed = useSelector((state) => state.authed)
  return authed ? <Outlet /> : <Navigate to="/login" />
}

export default PrivateOutlet
