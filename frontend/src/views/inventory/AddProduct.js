import React, { useState } from 'react'
import swal from '@sweetalert/with-react'

import {
  CButton,
  CCard,
  CCol,
  CCardBody,
  CCardHeader,
  CForm,
  CFormLabel,
  CFormInput,
  CFormTextarea,
} from '@coreui/react'
import axios from 'axios'
import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'

// const animatedComponents = makeAnimated()
const AddProduct = () => {
  const options = [
    { value: 'pcs', label: 'PCS' },
    { value: 'roll', label: 'ROLL' },
    { value: 'sheet', label: 'SHEET' },
  ]
  const token = useSelector((state) => state.currentUser.currentUser)

  const history = useNavigate()
  const [productCode, setProductCode] = useState('')
  const [productName, setProductName] = useState('')
  const [description, setDescription] = useState('')
  const [price, setPrice] = useState('')
  const [uom, setUom] = useState()

  const handleSubmit = async (e) => {
    e.preventDefault()
    try {
      await axios
        .post(
          'http://localhost:5000/api/v1/product/',
          {
            code: productCode,
            product_name: productName,
            description,
            price,
            uom,
          },
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          },
        )
        .then((data) => swal('Good job!', 'Product added!', 'success'))
      history('/inventory/product')
    } catch (error) {
      console.error(error)
    }
  }
  return (
    <>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>Add Product</strong>
          </CCardHeader>
          <CCardBody>
            <>
              <CForm className="row" onSubmit={handleSubmit} method="POST">
                <CCol md={4}>
                  <CFormLabel htmlFor="productCode">Product Code</CFormLabel>
                  <CFormInput
                    type="text"
                    name="productCode"
                    id="productCode"
                    value={productCode}
                    onChange={(e) => setProductCode(e.target.value)}
                    placeholder="input product code"
                    required
                  />
                </CCol>
                <CCol md={4}>
                  <CFormLabel htmlFor="productName">Product Name</CFormLabel>
                  <CFormInput
                    type="text"
                    name="productName"
                    id="productName"
                    value={productName}
                    onChange={(e) => setProductName(e.target.value)}
                    placeholder="Input product name"
                    required
                  />
                </CCol>
                <CCol md={4}>
                  <CFormLabel htmlFor="price">Price</CFormLabel>
                  <CFormInput
                    type="number"
                    name="price"
                    min={0}
                    max={999999999}
                    id="price"
                    value={price}
                    onChange={(e) => setPrice(e.target.value)}
                    placeholder="Input price"
                    required
                  />
                </CCol>
                <CCol md={12}>
                  <CFormLabel htmlFor="decription">Description</CFormLabel>
                  <CFormTextarea
                    type="text"
                    name="description"
                    id="decription"
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                    placeholder="description"
                    required
                  />
                </CCol>
                <CCol>
                  <CFormLabel htmlFor="decription">Unit of Measure</CFormLabel>
                  <select
                    className="form-control"
                    name="uom"
                    id="uom"
                    onChange={(e) => setUom(e.target.value)}
                  >
                    {options.map((opt) => (
                      <option key={opt.value} value={opt.value}>
                        {opt.label}
                      </option>
                    ))}
                  </select>
                  <CCol className="text-center">
                    <CButton md={3} type="submit" className="mt-2 text-center">
                      Save
                    </CButton>
                  </CCol>
                </CCol>
              </CForm>
            </>
          </CCardBody>
        </CCard>
      </CCol>
    </>
  )
}

export default AddProduct
