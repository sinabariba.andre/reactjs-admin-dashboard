import express from "express";
import userController from "../controllers/userController";
import Middleware from "../middleware";
import Validator from "../validator/index";

const router = express.Router()


router.get('/', Middleware.isAuthenticated, userController.getUser)
router.get('/:id', Middleware.isAuthenticated, userController.getUserById)
router.post('/',  Validator.checkCreateUser(), Middleware.handlerValidationError, userController.createUser)
router.patch('/:id', Middleware.isAuthenticated, Validator.checkIdParam(), Validator.checkCreateUser(), Middleware.handlerValidationError, userController.updateUser)
router.delete('/:id', Middleware.isAuthenticated, userController.deleteUser)


export default router