import express from "express";
import router from "./routes/index.route";
import db from "./config/db.config";
import cors from "cors";

const port = 5000;
const app = express();

db.sync().then(() => {
  console.log("db connected");
});

app.use(express.json());
app.use(cors());

app.use("/api/v1", router);

app.listen(port, () => console.log(`running on port ${port}`));
