import { Sequelize } from "sequelize-typescript";

// config db 
const db = new Sequelize("db_inventory", "root", "", {
  dialect: "mysql",
  logging: false,
});

export default db;
