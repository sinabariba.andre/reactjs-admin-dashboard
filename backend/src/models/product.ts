import { DataTypes, Model } from "sequelize";
import db from "../config/db.config";

class Product extends Model {}

Product.init(
  {
    code: { type: DataTypes.STRING, allowNull: false },
    product_name: { type: DataTypes.STRING, allowNull: false },
    description: { type: DataTypes.STRING, allowNull: false },
    price: { type: DataTypes.STRING, allowNull: false },
    uom: { type: DataTypes.STRING, allowNull: false },
  },
  {
    sequelize: db,
    modelName: "products",
  }
);

export default Product;
