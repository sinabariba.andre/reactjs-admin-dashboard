import React from 'react'
import CIcon from '@coreui/icons-react'
import { cilFile, cilSpeedometer } from '@coreui/icons'
import { CNavItem, CNavTitle } from '@coreui/react'

const _nav = [
  {
    component: CNavItem,
    name: 'Dashboard',
    to: '/',
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    badge: {
      color: 'info',
    },
  },
  {
    component: CNavTitle,
    name: 'Inventory',
  },
  {
    component: CNavItem,
    name: 'Product',
    to: '/inventory/product',
    icon: <CIcon icon={cilFile} customClassName="nav-icon" />,
  },
]

export default _nav
