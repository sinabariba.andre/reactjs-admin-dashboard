import React, { useEffect } from 'react'
import swal from '@sweetalert/with-react'
import {
  CButton,
  CCard,
  CCol,
  CCardBody,
  CCardHeader,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
  CSpinner,
  CForm,
  CFormInput,
} from '@coreui/react'

import CIcon from '@coreui/icons-react'
import { cilPen, cilPlus, cilTrash } from '@coreui/icons'

import { Link } from 'react-router-dom'
import axios from 'axios'
import { useDispatch, useSelector } from 'react-redux'

const Product = () => {
  const token = useSelector((state) => state.currentUser.currentUser)
  const dispacth = useDispatch()
  let no = 1
  const getProduct = () => {
    axios
      .get('http://localhost:5000/api/v1/product', {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => dispacth({ type: 'GET_PRODUCT', product: response.data }))
    // return data
  }
  const product = useSelector((state) => state.product.product)

  const deleteProduct = async (e) => {
    e.preventDefault()
    const id = e.target[0].value

    swal({
      title: 'Are you sure?',
      text: 'Once deleted, you will not be able to recover this prodcut!',
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        try {
          axios
            .delete(`http://localhost:5000/api/v1/product/${id}`, {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
            .then((data) => {})
        } catch (error) {
          console.error(error)
        }
        swal('Product has been deleted!', {
          icon: 'success',
        })
      }
    })
  }

  useEffect(() => {
    const interval = setInterval(() => getProduct(), 1000)

    return () => {
      clearInterval(interval)
    }
  })

  return !product ? (
    <CSpinner color="primary" />
  ) : (
    <>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>Data Product</strong>
          </CCardHeader>
          <Link to="/inventory/product/add" className="btn-primary">
            <CButton color="primary" xs={4} className="m-2 btn-sm">
              <CIcon icon={cilPlus} className="me-2" />
              Add Product
            </CButton>
          </Link>
          <CCardBody>
            <>
              <CTable>
                <CTableHead>
                  <CTableRow>
                    <CTableHeaderCell scope="col">No</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Product Code</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Product Name</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Price</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Description</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Unit of Measure</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Action</CTableHeaderCell>
                  </CTableRow>
                </CTableHead>
                <CTableBody>
                  {product ? (
                    product.data.map((item, index) => (
                      <CTableRow key={index}>
                        <CTableHeaderCell>{no++}</CTableHeaderCell>
                        <CTableDataCell>{item.code}</CTableDataCell>
                        <CTableDataCell>{item.product_name}</CTableDataCell>
                        <CTableDataCell>{item.price}</CTableDataCell>
                        <CTableDataCell>{item.description}</CTableDataCell>
                        <CTableDataCell>{item.uom}</CTableDataCell>
                        <CTableDataCell>
                          <Link to={`/inventory/product/${index}/edit`}>
                            <CButton color={'warning'} className="btn btn-sm mx-1">
                              <CIcon icon={cilPen} />
                            </CButton>
                          </Link>
                          <CForm onSubmit={deleteProduct} className="d-inline">
                            <CFormInput type="hidden" name="id" value={item.id} />
                            <CButton
                              color={'danger'}
                              className="btn btn-sm mx-1 d-inline"
                              type="submit"
                            >
                              <CIcon icon={cilTrash} />
                            </CButton>
                          </CForm>
                        </CTableDataCell>
                      </CTableRow>
                    ))
                  ) : (
                    <CTableDataCell>No Data</CTableDataCell>
                  )}
                </CTableBody>
              </CTable>
            </>
          </CCardBody>
        </CCard>
      </CCol>
    </>
  )
}

export default Product
