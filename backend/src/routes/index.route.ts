import express, { Request, Response } from "express";
import userRouter from "./user.route";
import productRouter from "./product.route";
import authRouter from "./auth.route";
const router = express.Router();

router.get("/", (req: Request, res: Response) => res.json("Welcome to API"));

router.use("/user", userRouter);
router.use("/product", productRouter);
router.use("/auth", authRouter);

export default router;
