import { Request, Response } from "express";
import { v4 as uuidv4 } from "uuid";
import Product from "../models/product";

//  get all product
const getProduct = async (req: Request, res: Response) => {
  try {
    const user = req.body.locals;
    // check admin
    if (user.role != "admin") {
      return res.status(401).json({ msg: "restricted " });
    }

    const page: number = parseInt(req.query.page as any) || 1;
    const perPage = 5;
    const total = await Product.count();

    const products = await Product.findAll({
      where: {},
      offset: (page - 1) * perPage,
    });

    return res.json({
      data: products,
      page,
      total,
      lastPage: Math.ceil(total / perPage),
    });
  } catch (error) {
    return res.json({ msg: error });
  }
};

// create product
const createProduct = async (req: Request, res: Response) => {
  try {
    const user = req.body.locals;
    const id = uuidv4();

    // check admin
    if (user.role != "admin") {
      return res.status(401).json({ msg: "restricted " });
    }
    const data = await Product.create({ ...req.body, id });
    return res.json({ data, msg: "product has been created" });
  } catch (err) {
    return res.json({
      msg: "fail to create product",
      status: 500,
      route: "/",
    });
  }
};

// get product by id
const getProductById = async (req: Request, res: Response) => {
  try {
    const user = req.body.locals;
    if (user.role != "admin") {
      return res.status(401).json({ msg: "restricted " });
    }
    const { id } = req.params;
    const product = await Product.findByPk(id);
    return res.json({ product });
  } catch (error) {
    res.json({ msg: `product not found` });
  }
};

// update product
const updateProduct = async (req: Request, res: Response) => {
  try {
    const user = req.body.locals;
    if (user.role != "admin") {
      return res.status(401).json({ msg: "restricted " });
    }
    const { id } = req.params;
    const data = await Product.findOne({ where: { id } });
    if (!data) {
      res.json({ msg: `fail to update product` });
      return false;
    }

    const updatedata = await data.update({ ...req.body });
    return res.json({
      updatedata,
      msg: `the product has been updated`,
    });
  } catch (err) {
    return res.json({ msg: "fail to updated", status: 500 });
  }
};

// delete product
const deleteProduct = async (req: Request, res: Response) => {
  try {
    const user = req.body.locals;
    if (user.role != "admin") {
      return res.status(401).json({ msg: "restricted " });
    }
    const { id } = req.params;
    const data = await Product.findOne({ where: { id } });
    if (!data) {
      return res.json({ msg: "cannot find the product" });
    }
    const deleteddata = await data.destroy();
    return res.json({ deleteddata, msg: "product has been deleted" });
  } catch (err) {
    return res.json({ msg: "fail to delete product", status: 500 });
  }
};

export default {
  getProduct,
  getProductById,
  createProduct,
  updateProduct,
  deleteProduct,
};
