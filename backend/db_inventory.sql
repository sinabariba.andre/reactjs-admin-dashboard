-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2022 at 01:45 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.3.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `uom` varchar(255) NOT NULL,
  `createdAt` date NOT NULL,
  `updatedAt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `code`, `product_name`, `description`, `price`, `uom`, `createdAt`, `updatedAt`) VALUES
('40b15419-7b1d-4b25-9ea0-844e5c23dcae', 'KJA001', 'Barang 1', 'ini adalah description', 9999, 'pcs', '2022-06-10', '2022-06-11'),
('548658fc-cdaa-4a5b-8796-7d0026592be3', 'KJA002', 'Barang 2', 'ini adalah description', 121, 'roll', '2022-06-11', '2022-06-11'),
('57ea6e6f-2c79-4512-8f0d-09e4368eee68', 'KJA003', 'barang 8', 'ini adalah description', 8228, 'sheet', '2022-06-11', '2022-06-11'),
('7f9a6788-9ce2-467e-8c72-789a79088686', 'KJA005', 'Barang 5', 'ini adalah description', 99299, 'sheet', '2022-06-11', '2022-06-11'),
('91d5442b-5346-4d84-b96a-a7e1e5c80dc1', 'KJA008', 'Barang 8 edited', 'ini adalah description', 81921, 'sheet', '2022-06-11', '2022-06-11'),
('92089b46-46ea-46cd-ae50-3aca88e5340b', 'KJA006', 'Barang  6 edited', 'ini adalah description', 8291, 'sheet', '2022-06-11', '2022-06-11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
