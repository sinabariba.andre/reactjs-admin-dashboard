import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";
import jwt from "jsonwebtoken";

class Middleware {
  handlerValidationError(req: Request, res: Response, next: NextFunction) {
    const error = validationResult(req);
    if (!error.isEmpty()) {
      return res.json(error.array()[0]);
    }
    next();
  }

  // authentication user
  isAuthenticated(req: Request, res: Response, next: NextFunction) {
    const authHeader: any = req.get("Authorization");
    if (!authHeader) {
      return res.status(401).json({ mgs: "unauthenticated" });
    }
    const token = authHeader.split(" ")[1];

    // verify jwt
    let decodedToken;
    try {
      decodedToken = jwt.verify(token, "secretKey");
    } catch (error) {
      return res.status(401).json({ mgs: "unauthenticated" });
    }
    if (!decodedToken) {
      return res.status(401).json({ mgs: "unauthenticated" });
    }
    req.body.locals = decodedToken;
    next();
  }
}
export default new Middleware();
