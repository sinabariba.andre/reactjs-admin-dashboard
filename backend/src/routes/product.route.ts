import express from "express";
import productController from "../controllers/productController";
import Middleware from "../middleware";
import Validator from "../validator/index";
const router = express.Router();

router.get(
  "/",
  Middleware.isAuthenticated,
  Middleware.handlerValidationError,
  productController.getProduct
);
router.get(
  "/:id",
  Middleware.isAuthenticated,
  Validator.checkIdParam(),
  Middleware.handlerValidationError,
  productController.getProductById
);
router.post(
  "/",
  Middleware.isAuthenticated,
  Validator.checkCreateProduct(),
  Middleware.handlerValidationError,
  productController.createProduct
);
router.patch(
  "/:id",
  Middleware.isAuthenticated,
  Validator.checkCreateProduct(),
  Middleware.handlerValidationError,
  productController.updateProduct
);
router.delete(
  "/:id",
  Middleware.isAuthenticated,
  Validator.checkIdParam(),
  Middleware.handlerValidationError,
  productController.deleteProduct
);

export default router;
