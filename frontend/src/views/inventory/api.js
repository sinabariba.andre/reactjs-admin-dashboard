/* eslint-disable prettier/prettier */
/* eslint-disable import/no-anonymous-default-export */
/* eslint-disable prettier/prettier */
import axios from 'axios'

export const getProduct = async (token) => {
  const res = await axios
    .get('http://localhost:5000/api/v1/product', {
      headers: { Authorization: `Bearer ${token}` },
    })
    // .then((response) => dispacth({ type: 'GET_PRODUCT', product: response.data }))
    
  return res
}

export default { getProduct }
