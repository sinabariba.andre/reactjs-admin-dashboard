import { body, param, query } from "express-validator";

class Validator {
  checkCreateUser() {
    return [
      body("username")
        .notEmpty()
        .withMessage("This field cannot be null")
        .isLength({ min: 3 })
        .withMessage("username too short"),
      body("password")
        .matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/, "i")
        .withMessage(
          "password must be contain min 8 character, Uppercase, lowercase and number "
        ),
    ];
  }

  checkCreateProduct() {
    return [
      body("code").notEmpty().withMessage("This field cannot be null"),
      body("product_name").notEmpty().withMessage("This field cannot be null"),
      body("description").notEmpty().withMessage("This field cannot be null"),
      body("price").notEmpty().withMessage("This field cannot be null"),
      body("uom").notEmpty().withMessage("This field cannot be null"),
    ];
  }

  checkPagination() {
    return [
      query("limit")
        .notEmpty()
        .withMessage("The query limit should be not empty")
        .isInt({ min: 1, max: 10 })
        .withMessage("limit value should be number and between 1-10"),
    ];
  }

  checkIdParam() {
    return [
      param("id")
        .notEmpty()
        .withMessage("the value should be not empty")
        .isUUID(4)
        .withMessage("the value should be uuid v4"),
    ];
  }
}
export default new Validator();
